import React, { Component } from 'react';

class CalendarFirstWeek extends Component {
		
	static propTypes() {
		baseDate: React.PropTypes.objectOf(new Date());
		date: React.PropTypes.objectOf(new Date());
		json: React.PropTypes.object;
	}

	constructor(props) {
		super(props);
		this.state = {
			days: ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
			days_count: ["31", "0", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"],
			mon_index: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
		}
	}

	render(){  
		this.state.days_count[1] = this.props.date.getFullYear() % 4 == 0 ? "29" : "28";
		let g = new Date();
		g.setDate(1);
		g.setMonth(this.props.date.getMonth());
		g.setFullYear(this.props.date.getFullYear());

		let curr_mon = g.getMonth();
		let prev_mon = curr_mon == 0 ? 11 : curr_mon-1;
		let last_day_of_prev_mon = this.state.days_count[prev_mon];		
		let weekday = g.getDay();					
		weekday = weekday==0?6:weekday;
		let arr = [];
		for(let i = 1; i < weekday; i++) {
			arr[i-1] = last_day_of_prev_mon - weekday + i + 1;
		}
		for(let i = weekday; i <= 7; i++) {
			arr[i-1] = i-weekday+1;
		}	
		return 	<div className='calendar__week'>
				{
					arr.map((e, i)=>{
						let _class = '';
						let date = [];
						let month = this.props.date.getMonth();
						let year = this.props.date.getFullYear();

						e > 8 	&& (_class += " " + "day_of_prev_mon") 
								&& (date[0] = e) 
								&& (date[1] = month==0 ? "12" : this.state.mon_index[month-1])
								&& (date[2] = month==0 ? year-1 : year);

						i > 4 && (_class += " " + "calendar__holiday");
						
						let isToday = e < 8 && e == this.props.baseDate.getDate() 
											&& month == this.props.baseDate.getMonth() 
											&& year == this.props.baseDate.getFullYear();
						
						e < 8 && (date[0] = "0" + e) && (date[1] = this.state.mon_index[month]) && (date[2] = year);
						
						isToday && (_class += " " + "calendar__today");	
						return <div key={ i } 
									className={ _class.trim() } 
									data-current={ date.join('.') }
									onClick={ this.props.handleClick }
									>
									{ 
										this.state.days[i] + ', ' + e 
									}
								</div>
					})
				}
				</div>
	}
}

export default CalendarFirstWeek;