import React, { Component } from 'react';

class CalendarFormButtons extends Component {
	render() {
		if(this.props.loader) {
			return <span style={{ textAlign: 'center', display: 'block', marginTop: -9}}><img src="loader.gif" style={{ width: 25, height: 25 }}/></span>
		} else {			
			return [
				<button key={1} type="button" name="ready" onClick={ this.props.ready }>Готово</button>,
				<span key={2}>&nbsp;</span>,
				<button key={3} type="button" name="delete" onClick={ this.props.delete }>Удалить</button>
			]
		}
	}
}

export default CalendarFormButtons;
