/*jshint esversion: 6 */
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux' 
import { createStore } from 'redux' 
 
import Calendar from './Calendar.jsx'

const CalendarInitialState = {
	url: '',
	php: '',
	baseDate: null,
	currentDate: null,
	preLoad: true,
	error: null,
	json: null,
	searchResultsShow: false,   
	searchResultsArr:[],
	mobile: window.innerWidth <= 900,
	formShow: false,
	formOffsetLeft: 0,
	formOffsetTop: 0,  
	formClass: '',
	formData: {},
	formDate:'',
	formLoader: false,
	formError: false   
}

let CalendarReducer = (state = CalendarInitialState, action)=>{																		

	let newState = {};
	switch (action.type) {														
		case 'FORM_SHOW_MOBILE':
			newState = {									
				formShow: true,
				formDate: action.data.date,
				formData: state.json[action.data.date] || {},  
				formError: false,
				formOffsetTop: window.pageYOffset + 90,
				formOffsetLeft: 0,
				formClass: '',
				mobile: true
			}
			return Object.assign({}, state, newState);	
		case 'FORM_SHOW_DESKTOP':
			newState = {									
				formShow: true,
				formDate: action.data.date,
				formData: state.json[action.data.date] || {},  
				formError: false,
				formOffsetTop: action.data.top,
				formOffsetLeft: action.data.left,
				formClass: 'calendar-form-' + action.data.pos,
				mobile: false
			}
			return Object.assign({}, state, newState);			
		case 'FORM_HIDE':
			newState = {
				formShow: false,
				formData: {},
				formError: false,
				formLoader: false
			}
			return Object.assign({}, state, newState);	
		case 'SEARCH_RESULTS_SHOW':
			newState = {
				searchResultsArr: action.data,
				searchResultsShow: true
			}	
			return Object.assign({}, state, newState);	
		case 'SEARCH_RESULTS_HIDE':
			newState = {
				searchResultsArr: [],
				searchResultsShow: false
			}	
			return Object.assign({}, state, newState);
		case 'GOTO_DATE': 
			newState = {
				searchResultsShow: false,
				currentDate: action.data
			}	
			return Object.assign({}, state, newState);													
		case 'FORM_LOADER':
			newState = {
				formLoader: true
			}	
			return Object.assign({}, state, newState);
		case 'FORM_ERROR':
			newState = {
				formLoader: false,
				formError: action.data
			}
			return Object.assign({}, state, newState);
		case 'FORM_SEND_SUCCESS':
			newState = {
				json: action.data,
				formLoader: false,
				formShow: false
			}
			return Object.assign({}, state, newState);
		case 'MOBILE_TRUE':
			newState = {
				mobile: true
			}
			return Object.assign({}, state, newState);
		case 'MOBILE_FALSE':
			newState = {
				mobile: false
			}
			return Object.assign({}, state, newState);
		case 'ERROR':
			newState = {
				preLoad: false,
				error: action.data
			}
			return Object.assign({}, state, newState);
		case 'INIT':
			newState = {
				preLoad: false,
				json: action.data.json,
				baseDate: action.data.date,
				currentDate: action.data.date,
				url: action.data.url,
				php: action.data.php 
			}
			return Object.assign({}, state, newState);
	}

	return state;
}

let store = createStore(CalendarReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

store.subscribe(()=>{
	console.log('subscribe', store.getState());	  					
})

class App extends React.Component {

	render(){
		return <Provider store={ store }>
					<Calendar />
				</Provider>

	}
}

render(<App />, document.getElementById("app"))
