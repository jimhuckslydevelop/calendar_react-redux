
import React, { Component } from 'react';
import { connect } from 'react-redux';

import CalendarForm from './CalendarForm.jsx';
import CalendarSearchResults from './CalendarSearchResults.jsx';
import CalendarHeader from './CalendarHeader.jsx';
import CalendarBody from './CalendarBody.jsx';

class Calendar extends Component {

	constructor(props) {  
		super(props);	
		this.getNativeDate = this.getNativeDate.bind(this);						
	}

	getNativeDate(date) {
		return date.getDate instanceof Function ? date :
			typeof date === 'string' ? new Date(date.replace(/(\d+).(\d+).(\d+)/g, '$2/$1/$3')) : null
	}

	handleClick(e){	
		if(! (e.target.className == 'calendar-form' || e.target.closest('.calendar-form') !== null) ){				
			if(e.target.closest('.calendar__week') == null) {
				this.props.dispatch('FORM_HIDE');	
			}
		}
	}

	handleBackBtn() {
		let curr_mon = this.props._store.currentDate.getMonth(),
			curr_year = this.props._store.currentDate.getFullYear();

		let mon = curr_mon == 0 ? 11 : curr_mon-1;
		let year = curr_mon == 0 ? curr_year-1 : curr_year;
		let g = new Date();

		g.setDate(this.props._store.currentDate.getDate());
		g.setMonth(mon);
		g.setFullYear(year);

		this.props.dispatch('GOTO_DATE', g);
	}

	handleForwardBtn() {
		let curr_mon = this.props._store.currentDate.getMonth(),
			curr_year = this.props._store.currentDate.getFullYear();

		let mon = curr_mon == 11 ? 0 : curr_mon+1;
		let year = curr_mon == 11 ? curr_year+1 : curr_year;
		let g = new Date();

		g.setDate(this.props._store.currentDate.getDate());
		g.setMonth(mon);
		g.setFullYear(year);

		this.props.dispatch('GOTO_DATE', g);
	}

	handleTodayBtn(){
		this.props.dispatch('GOTO_DATE', this.props._store.baseDate);
	}

	handleSearch(e){
		let json = this.props._store.json;
		let value = e.target.value.toUpperCase().trim();
		let arr=[];
		Object.keys(json).forEach((e,i)=>{
			if(value.length) {
				if(json[e].event.toUpperCase().indexOf(value) > -1 || json[e].description.toUpperCase().indexOf(value) > -1) {
					arr.push(e);
				}
			}
		});

		if(arr.length) {
			this.props.dispatch('SEARCH_RESULTS_SHOW', arr);
		} else {
			this.props.dispatch('SEARCH_RESULTS_HIDE');
		}
	}

	handleSearchResultsClose(){
		let input = document.getElementById('Search');
		input.value = '';
		input.focus();
		this.props.dispatch('SEARCH_RESULTS_HIDE');
	}

	handleSearchResultsClick(e){
		let date = e.target.closest('span').attributes['data-current'].value;
		let g = this.getNativeDate(date);
		document.getElementById('Search').value = '';
		this.props.dispatch('GOTO_DATE', g);		
	}

	handleElementClick(e){
		let winW = window.innerWidth;
		let calendarW = document.getElementById('calendar-body').offsetWidth;
		let calendarH = document.getElementById('calendar-body').offsetHeight;
		let absLeft = (winW - calendarW)/2;
		let top = e.pageY - e.target.closest('#calendar-body').offsetTop;
		let left = e.pageX - e.target.closest('#calendar-body').offsetLeft - absLeft;
		let posY = ["top", "bottom"];
		let posX = ["left", "right"];

		posY = top < calendarH/2 ? posY[0] : posY[1];
		posX = left < calendarW/2 ? posX[0] : posX[1];
		let pos = [posY, posX].join('-');

		let date = e.target.closest('div').attributes['data-current'].value;		

		switch (pos) {
			case 'top-left':
				top = top-15;
				left = left+25;
			break;
			case 'top-right':
				top = top-15;
				left = left-325;
			break;
			case 'bottom-left':
				top = top-255;
				left = left+25;
			break;
			case 'bottom-right':
				top = top-255;
				left = left-325;
			break;
		}

		if(this.props._store.mobile) {
			this.props.dispatch('FORM_SHOW_MOBILE', {date: date});
		} else {
			this.props.dispatch('FORM_SHOW_DESKTOP', {top: top, left: left, pos: pos, date: date});
		}
		
		let self = this;

		window.addEventListener('resize', ()=>{
			if(window.innerWidth <= 900) {
				if(self.props._store.formShow) {
					self.props.handler('FORM_SHOW_MOBILE', {date: date});
				}				
			} else {
				if(self.props._store.formShow) {
					self.props.handler('FORM_SHOW_DESKTOP', {top: top, left: left, pos: pos, date: date});
				}
			}
		})
	}

	handleFormClose(){
		this.props.dispatch('FORM_HIDE');
	}

	handleReady(){
		let form  = document.getElementById("add-event-form");
		let elements = form.elements;
		let params = {};
		for(var i = 0; i < form.elements.length; i++) {
			if (elements[i].tagName == 'INPUT' || elements[i].tagName == 'TEXTAREA') {
				params[elements[i].name] = elements[i].value;
			}
		}
		params['mode'] = 'add'; 
		let data = new FormData();
		data.append('json', JSON.stringify(params));
		
		this.props.dispatch('FORM_LOADER');

		fetch(this.props._store.php, {
			method: 'POST',
			body: data
		})
		.then((data)=>{		
			data.json()
			.then((data)=>{
				if(['error'] in data) {
					if(['message'] in data.error) {
						this.props.dispatch('FORM_ERROR', 'Хотя бы одно поле должно быть заполнено');						
					} else {	
						this.props.dispatch('FORM_ERROR', data.error.message);												
					}
				} else {
					let json = this.props._store.json;
					json[params['date']] = {
						'event' : params['event'],
						'persons' : params['persons'],
						'description' : params['description']
					}
					this.props.dispatch('FORM_SEND_SUCCESS', json);					
				}
			})
			.catch((error)=>{
				console.log(error);
				this.props.dispatch('FORM_HIDE');
			});
		})
		.catch((error)=>{
			console.log(error);
			this.props.dispatch('FORM_HIDE');
		})
	}

	handleDelete(){
		let form  = document.getElementById("add-event-form");
		let params = {};
		params['date'] = form.elements['date'].defaultValue;
		params['mode'] = 'delete';
		let data = new FormData();
		data.append('json', JSON.stringify(params));

		this.props.dispatch('FORM_LOADER');

		fetch(this.props._store.php, {
			method: 'POST',
			body: data
		})
		.then((data)=>{
			data.json()
			.then((data)=>{
				if(['success'] in data) {
					let json = this.props._store.json;
					Object.keys(json).forEach((e, i)=>{
						if(e == params['date']) {
							delete json[e];
						}
					});
					this.props.dispatch('FORM_SEND_SUCCESS', json);						
				} else {
					if(['error'] in data && ['message'] in data.error) {
						this.props.dispatch('FORM_HIDE');
					}
				}			
			})
			.catch((error)=>{
				console.log(error); 
				this.props.dispatch('FORM_HIDE'); 
			})
		})
	}

	componentDidMount(){
		fetch(this.props.url || "./events.json?v="+Math.random())
			.then((response)=>{  		
				if(response.status !== 200){
					this.props.dispatch('ERROR', 'Looks like there was a problem. Status Code: ' +  response.status);
			} else {
				response.json().then((data)=>{
					if(["Events"] in data) { 
						let date = this.getNativeDate(this.props.date || new Date());
						date && this.props.dispatch('INIT', { 	json: data["Events"], 
																date: date, 
																url: this.props.url || "./events.json?v="+Math.random(), 
																php: this.props.php || "./events.php" });
						!data && this.props.dispatch('ERROR', 'Date is invalid');		
					} else {
						this.props.dispatch('ERROR', "Fail load from " + this.props.url);
					}
				});
			}
		})
		.catch((error)=>{
			this.props.dispatch('ERROR', 'Fetch Error : ' + error);
		});
	}

	render(){
		this.state = this.props._store;
		let self = this;

		window.addEventListener('resize', ()=>{
			if(window.innerWidth <= 900) {
				if(!self.props._store.mobile) {
					self.props.handler('MOBILE_TRUE')
				}				
			} else {
				if(self.props._store.mobile) {
					self.props.handler('MOBILE_FALSE')
				}
			}
		});

		if(this.props._store.preLoad) {
			return <div style={{ textAlign: "center", padding: "25px 0px" }}>
						<img src="loader.gif" alt="" width="50" height="50" />
					</div>
		}

		if(!this.props._store.error) {
			return 	<div className="calendar-wrap" onClick={ this.handleClick.bind(this) }>
						<div className="calendar">
							<div className="calendar-search">
								<div>									          
									<span className="fa fa-search"></span>
				    				<input type="text" id="Search" onKeyUp={ this.handleSearch.bind(this) }/>
				    				<span className="fa fa-close" onClick={ this.handleSearchResultsClose.bind(this) }></span>
				    			</div>
				    			<CalendarSearchResults 	isShow={ this.state.searchResultsShow } 
				    									arr={ this.state.searchResultsArr} 
				    									json={ this.state.json }
				    									handleClick={ this.handleSearchResultsClick.bind(this) }
				    									/>
							</div>
							<div className="calendar-nav">
								<button className="calendar__back" onClick={ this.handleBackBtn.bind(this) }>
									<span className="fa fa-caret-left"></span>
								</button>
								<CalendarHeader date={ this.state.currentDate } baseDate={ this.state.baseDate } />
								<button className="calendar__forward" onClick={ this.handleForwardBtn.bind(this) }>
									<span className="fa fa-caret-right"></span>
								</button>
								<button className="calendar__today-btn" onClick={ this.handleTodayBtn.bind(this) }>Сегодня</button>
							</div>								
							<CalendarBody 	date={ this.state.currentDate } 
											baseDate={ this.state.baseDate } 
											json={ this.state.json }
											handleClick={ this.handleElementClick.bind(this) }
											/>
							<CalendarForm 	isShow={ this.state.formShow } 
											left={ this.state.formOffsetLeft }
											top={ this.state.formOffsetTop }
											_class={ this.state.formClass }
											handleClose={ this.handleFormClose.bind(this) }
											data={ this.state.formData }
											date={ this.state.formDate }
											ready={ this.handleReady.bind(this) }
											delete={ this.handleDelete.bind(this) }
											loader={ this.state.formLoader }
											error={ this.state.formError }
											/>
						</div>										
					</div>
		} else {
			return 	<div style={{ color: "red", padding: "5px" }}>
					{
						this.props._store.error       
					}
					</div>
		}
	}
}

export default connect(
					state => ({_store: state }),

					dispatch => ({

						dispatch: (type, data)=>{
							dispatch({
								type: type,
								data: data || null				
							})
						}	
					})
				)(Calendar)