import React, { Component } from 'react';

class CalendarSearchResults extends Component {

	static propsTypes() {
		isShow: React.PropTypes.bool;
		arr: React.PropTypes.array;
		json: React.PropTypes.object;
		handleClick: React.PropTypes.func;
	}

	constructor(props) {
		super(props);

		this.state = {
			month: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"]
		}
	}
	
	render(){
		if(this.props.isShow) {
			return 	<div className="calendar-search__results">
					{						
						this.props.arr.map((e,i)=>{
							let date = e.split('.');
							return 	<div key={ i } onClick={ this.props.handleClick }>
										<span data-current={ e }>
											<b>
											{
												this.props.json[e].event
											}
											</b>
											<i>
												{
													date[0] + " " + this.state.month[parseInt(date[1])-1]
												}
											</i>
										</span>
									</div>
						})
					}
					</div>
		} else {
			return []
		}
	}
}

export default CalendarSearchResults;