import React, { Component } from 'react';
 
class CalendarFormError extends Component {
	render(){
		if(this.props.data) {
			return <span style={{ color: 'red', fontSize: '12px' }}>{ this.props.data }</span>
		} else {
			return []
		}

	}
}

export default CalendarFormError;