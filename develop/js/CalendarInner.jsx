import React, { Component } from 'react';

class CalendarInner extends Component {
		
	static propTypes() {
		baseDate: React.PropTypes.objectOf(new Date());
		date: React.PropTypes.objectOf(new Date());
		json: React.PropTypes.object;
	}

	constructor(props) {
		super(props);
		this.state = {
			days: ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
			days_count: ["31", "0", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"],
			mon_index: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],

			showDays: false
		}
	}

	render(){
		this.state.showDays = window.innerWidth <=690;
		window.addEventListener('resize', ()=>{
			window.innerWidth <=690 ? (!this.state.showDays && this.setState({
				showDays: true
			})) : (this.state.showDays && this.setState({
				showDays: false
			}))
		});

		this.state.days_count[1] = this.props.date.getFullYear() % 4 == 0 ? "29" : "28";
		let last_day = this.state.days_count[this.props.date.getMonth()];
		let curr_mon = this.props.date.getMonth(),
			curr_year = this.props.date.getFullYear();
		let g = new Date();

		g.setDate(1);
		g.setMonth(curr_mon);
		g.setFullYear(curr_year);

		let weekday = g.getDay();						
		weekday = weekday==0?6:weekday-1;
		let first_day = 7 - weekday + 1;
		let arr = [];
		return [
			[1,2,3,4,5].map((e,i)=>{				
				for(let k = 0; k < 7; k++) {
					arr[k] = first_day+7*i+k;
				}
				return 	<div className='calendar__week' key={ i }>
							{
								arr.map((o,j) => {
									let _class='';
									let date = [];
									let month = this.props.date.getMonth();
									let year = this.props.date.getFullYear();
									o <= last_day 	&& (date[0] = o > 9 ? o : "0" + o)
													&& (date[1] = this.state.mon_index[month])
													&& (date[2] = year);
									o > last_day 	&& (date[0] = o-last_day>9 ? o-last_day : "0" + (o-last_day)) 
													&& (date[1] = month == 11 ? "01" : this.state.mon_index[month+1])
													&& (date[2] = year+1)
													&& (_class += " " + "day_of_next_mon");
									let isToday =	o == this.props.baseDate.getDate() 
													&& month == this.props.baseDate.getMonth() 
													&& year == this.props.baseDate.getFullYear();
									isToday && (_class += " " + "calendar__today");
									date = date.join('.');
									let day = this.state.showDays ? this.state.days[j] + ', ' : '';
									if(date in this.props.json) {
										_class += " " + "calendar__event";										
										return <div key={ j }
													className={ _class.trim() } 
													data-current={ date }
													onClick={ this.props.handleClick }>
													<span>
														{ 														
															day + (o > last_day ? o-last_day : o)		
														}
													</span>
													<span className="calendar__event-header">
													{
														this.props.json[date].event
													}
													</span>
													<span className="calendar__event-descr">
													{
														this.props.json[date].description
													}
													</span>
												</div>
									} else {
										return <div key={ j } 
													className={ _class.trim() } 
													data-current={ date }
													onClick={ this.props.handleClick }>
													{ 
														day  + (o > last_day ? o-last_day : o) 
													}
												</div>
									}
								})
							}
						</div>
			})
		]
	}
}

export default CalendarInner;