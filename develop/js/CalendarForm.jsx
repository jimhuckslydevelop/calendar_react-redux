import React, { Component } from 'react';
import CalendarFormButtons from './CalendarFormButtons.jsx';
import CalendarFormError from './CalendarFormError.jsx';

class CalendarForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			event: props.data.event,
			date: props.data.date,
			persons: props.data.persons,
			description: props.data.description,

			change: false,

			loader1: false,
			loader2: false
		}

		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(f){
		return (e) => {
			this.setState({
				[f] : e.target.value,
				change: true
			})
		}
	}

	render(){
		if(this.props.isShow) {	
			if(!this.state.change) {
				this.state = {
					event: this.props.data.event,
					date: this.props.date,
					persons: this.props.data.persons,
					description: this.props.data.description
				}	
			} else {
				this.state.change = false;
			}
				
			return 	<div className={"calendar-form" + " " + this.props._class } style={{top: this.props.top, left: this.props.left}}>
						<div>
							<CalendarFormError data={ this.props.error }/>
							<form action="" id="add-event-form">
								<span className="fa fa-close" onClick={ this.props.handleClose }></span>
								<input type="text" name="event" placeholder="Событие" value={ this.state.event || '' } onChange={ this.handleChange('event') }/>
								<input type="text" name="date" placeholder="День, месяц, год" value={ this.state.date } readOnly />
								<input type="text" name="persons" placeholder="Имена участников" value={ this.state.persons || '' } onChange={ this.handleChange('persons') }/>
								<textarea name="description" placeholder="Описание" value={ this.state.description || '' } onChange={ this.handleChange('description') }></textarea>
								<CalendarFormButtons loader={ this.props.loader } ready={ this.props.ready } delete={ this.props.delete }/>								
							</form>
						</div>
					</div>
		} else {
			return []
		}
	}
}

export default CalendarForm;