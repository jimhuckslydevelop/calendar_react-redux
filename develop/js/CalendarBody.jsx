import React, { Component } from 'react';
import CalendarFirstWeek from './CalendarFirstWeek.jsx';
import CalendarInner from './CalendarInner.jsx';

class CalendarBody extends Component {
		
	static propTypes() {
		baseDate: React.PropTypes.objectOf(new Date());
		date: React.PropTypes.objectOf(new Date());
		json: React.PropTypes.object;
	}

	constructor(props) {
		super(props);
		this.state = {}
	}

	render(){
		return 	<div className="calendar-body" id="calendar-body">
					<CalendarFirstWeek 	date={ this.props.date } 
										baseDate={ this.props.baseDate } 
										json={ this.props.json } 
										handleClick={ this.props.handleClick }
										/>
					<CalendarInner 	date={ this.props.date } 
									baseDate={ this.props.baseDate } 
									json={ this.props.json } 
									handleClick={ this.props.handleClick }
									/>				
				</div>
	}
}

export default CalendarBody;