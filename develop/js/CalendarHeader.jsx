import React, { Component } from 'react';

class CalendarHeader extends Component {
		
	static propTypes() {
		baseDate: React.PropTypes.objectOf(new Date());
		date: React.PropTypes.objectOf(new Date());
	}

	constructor(props) {
		super(props);
		this.state = {
			month : ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
		}
	}

	render(){
		return	<div className='calendar__header'>
				{
					this.state.month[this.props.date.getMonth()] + ' ' + this.props.date.getFullYear()
				}
				</div>
	}
}

export default CalendarHeader;