
// jade

var $ 					= require('gulp-load-plugins')(),
	gulp 					= require('gulp'),
	jadeInheritance 	= require('gulp-jade-inheritance'),
	path 					= require('path'),
	combine 				= require('stream-combiner2').obj; // создает мультипоток

	var filter = 

module.exports =  (options) => {

	gulp.src(options.src)
		.pipe(gulp.dest(options.dest + 'jade/'));

	gulp.src(options.src.split('/')[0] + '/jade/*.jade')
		.pipe(gulp.dest(options.dest + '/jade/jade/'));

	return (callback) => {
		combine(
			gulp.src(options.src),
			$.changed(options.dest, {extension: '.html'}),
			jadeInheritance({basedir: options.src.split('/')[0]}),
			$.filter(function (file) {
	            return !/\/_/.test(file.path) && !/^_/.test(file.relative);
	        }),
			$.jade({
				pretty: true
			}),
			gulp.dest(options.dest)
			).on('error', $.notify.onError((err) => {

				return {
					title: 'JADE',
					message: err.message
				}	
			}));
		callback();
	}
}