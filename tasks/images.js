
// images

var $ 				= require('gulp-load-plugins')(),
	gulp 				= require('gulp'),
	combine 			= require('stream-combiner2').obj;

module.exports = (options) => {

	var args = options.args, isDevelopment = true;
	
	if(['dev'] in args || ['prod'] in args) {
		isDevelopment = ['dev'] in args;
	}

	var compress = ['full'] in args;

	if(!isDevelopment) {
		isDevelopment = !compress;
	}

	return (callback) => {
		combine(
			gulp.src(options.src),
			gulp.dest(options.dest)
		).on('error', $.notify.onError((err) => {

			return {
				title: 'IMAGES',
				message: err.message
			}
		}));
		callback();
	}
}