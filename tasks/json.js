
// json

var $ 				= require('gulp-load-plugins')(),
	gulp 			= require('gulp');

module.exports = (options) => {

	return (callback) => {
		gulp.src(options.src)
			.pipe(gulp.dest(options.dest));
		callback();
	}
}