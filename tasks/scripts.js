
// scripts

var $ 				= require('gulp-load-plugins')(),
	gulp 			= require('gulp'),	
	combine 		= require('stream-combiner2').obj; // создает мультипоток

module.exports =  (options) => {

	var args = options.args, isDevelopment = true;
	
	if(['dev'] in args || ['prod'] in args) {
		isDevelopment = ['dev'] in args;
	}

	return (callback) => {	
		// library
		combine(
			gulp.src(options.src_lib),
			$.concat('vendor.min.js'),
			$.if(!isDevelopment, $.uglifyjs()),
			gulp.dest(options.dest)
		).on('error', $.notify.onError((err) => {

			return {
				title: 'Library',
				message: err.message
			};
			this.end();
		}));
		// scripts
		combine(
			gulp.src(options.src),
			$.if(isDevelopment, $.newer(options.dest)),	
			gulp.dest(options.dest)
		).on('error', $.notify.onError((err) => {

			return {
				title: 'MY-SCRIPT',
				message: err.message
				};	
				this.end();
			}));	
		callback();
	}
}