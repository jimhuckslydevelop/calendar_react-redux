const path      = require('path'),
      webpack   = require('webpack'),
      NODE_ENV  = process.env.NODE_ENV;

var ExtractTextPlugin = require("extract-text-webpack-plugin"),
    plugins = [];

plugins.push(new ExtractTextPlugin("css/bundle.css"));

if(NODE_ENV == 'production') {
  plugins.push(new webpack.optimize.UglifyJsPlugin());
  plugins.push(new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production')
  }));
}

module.exports = {
  entry: "./develop/js/app.jsx",
  module: {
    loaders: [
    {
      test: /\.(js|jsx)$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react']
      }
    },
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        use: 'css-loader',
        fallback: 'style-loader'
      })
    }
    ]
  },
  plugins: plugins,
  output: {
    path: path.resolve(__dirname, './build/'),
    filename: 'js/bundle.js'
  }
};